import React from "react";

export default function BackButton() {
  return (
    <div>
      <button className="shadow">Back</button>
    </div>
  );
}
