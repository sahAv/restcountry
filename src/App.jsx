import { BrowserRouter, Route, Routes } from "react-router-dom";
import { useTheme } from "./components/ThemeContext";
import Header from "./components/Header";
import Countries from "./components/Countries";
import Country from "./components/Country";
import "./App.css";

export default function App() {
  const { darkMode } = useTheme();
  return (
    <div className={darkMode ? "app-dark" : "app-light"}>
      <BrowserRouter>
        <Header />
        <Routes>
          <Route path="/" element={<Countries />} />
          <Route path="country/:id" element={<Country />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}
