import React, { useState, useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import BackButton from "./BackButton";
import Data from "./countries.json";

const Country = () => {
  const [countryData, setCountryData] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  const { id } = useParams();

  useEffect(() => {
    setIsLoading(true);
    try {
      setTimeout(() => {
        const filteredData = Data.filter((data) => data.name.common === id);
        if (filteredData.length > 0) {
          setCountryData(filteredData[0]);
          setIsLoading(false);
        } else {
          setIsLoading(false);
          setError("Country not found.");
        }
      }, 1000);
    } catch (error) {
      setIsLoading(false);
      setError("Failed to fetch data. Please try again later.");
    }
  }, [id]);

  const findCountryName = (border) => {
    const country = Data.find(
      (item) =>
        item.cioc === border ||
        item.cca2 === border ||
        item.ccn3 === border ||
        item.cca3 === border
    );
    return country ? country.name.common : "";
  };

  return (
    <div className="countryContainer">
      <div className="backButton">
        <Link to="/">
          <BackButton />
        </Link>
      </div>

      {isLoading && <div className="notFound">Loading...</div>}

      {error && <div className="notFound">Error: {error}</div>}
      <div className="countryDiv">
        {countryData && (
          <>
            <img src={countryData.flags.png} alt="" />
            <div className="countryInnerBox">
              <h2>{countryData.name.common}</h2>
              <div className="countryInfo">
                <div className="leftBox">
                  <h3>
                    <span>Native Name: </span>
                    {Object.values(countryData.name.nativeName)
                      .map((lang) => lang.common)
                      .join(", ")}
                  </h3>
                  <h3>
                    <span>Population: </span> {countryData.population}
                  </h3>
                  <h3>
                    <span>Region: </span> {countryData.region}
                  </h3>
                  <h3>
                    <span>Sub Region: </span> {countryData.subregion}
                  </h3>
                  <h3>
                    <span>Capital: </span> {countryData.capital}
                  </h3>
                </div>
                <div className="rightBox">
                  <h3>
                    <span>Top Level Domain: </span> {countryData.tld}
                  </h3>
                  <h3>
                    <span>Currencies: </span>
                    {
                      countryData.currencies[
                        Object.keys(countryData.currencies)[0]
                      ].name
                    }
                  </h3>
                  <h3>
                    <span>Languages:</span>{" "}
                    {Object.values(countryData.languages).join(", ")}
                  </h3>
                </div>
              </div>
              <div className="BorderCountryDiv">
                <h3>
                  <span>BorderCountry: </span>
                </h3>
                <div className="borderCountry">
                  {countryData.borders && countryData.borders.length > 0 ? (
                    countryData.borders.map((border) => (
                      <h3 key={border}>{findCountryName(border)}</h3>
                    ))
                  ) : (
                    <h3>None</h3>
                  )}
                </div>
              </div>
            </div>
          </>
        )}
      </div>
    </div>
  );
};

export default Country;
