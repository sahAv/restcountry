export default function Sort({ onSelectSort, sortBy }) {
  const handleSorting = (e) => {
    onSelectSort(sortBy, e.target.value);
  };

  return (
    <>
      <select onChange={handleSorting}>
        <option value="">Sort by {sortBy}</option>
        <option key={"asc"} value={"asc"}>{"asc"}</option>
        <option key={"desc"} value={"desc"}>{"desc"}</option>
      </select>
    </>
  );
}
