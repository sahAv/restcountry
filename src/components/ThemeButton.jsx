import React from "react";
import { useTheme } from "./ThemeContext";

function ThemeButton() {
  const { darkMode, toggleTheme } = useTheme();
  return (
    <div>
      <button className="toogle-buttton" onClick={toggleTheme}>
        <i className={`fa-solid ${darkMode ? "fa-sun" : "fa-moon"}`}></i>
        {darkMode ? "Light Mode" : "Dark Mode"}
      </button>
    </div>
  );
}

export default ThemeButton;
