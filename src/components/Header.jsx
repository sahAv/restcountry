import { Link } from "react-router-dom";
import ThemeButton from "./ThemeButton";
export default function Header() {
  return (
    <div className="header">
      <Link to="/">
        <h5>Where in the world?</h5>
      </Link>
      <ThemeButton />
    </div>
  );
}
