import { useState } from "react";

export default function SearchInput({ onSearch }) {
  const [input, setInput] = useState("");

  function handleSubmit(e) {
    e.preventDefault();
    setInput(e.target.value);
    onSearch(e.target.value);
  }

  return (
    <input
      type="text"
      onChange={(e) => handleSubmit(e)}
      placeholder={'Search a country...'}
      value={input}
    />
  );
}
