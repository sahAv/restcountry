export default function FilterCountry({ onSelectRegion, regions, filterBy }) {
  const handleRegionChange = (e) => {
    onSelectRegion(e.target.value);
  };

  return (
    <>
      <select onChange={handleRegionChange}>
        <option value="">Filter by {filterBy}</option>
        {[...regions].map((region) => (
          <option key={region} value={region}>
            {region}
          </option>
        ))}
      </select>
    </>
  );
}
