import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Data from "./countries.json";
import SearchInput from "./SearchInput";
import FilterCountry from "./Filter";
import Sort from "./Sort";

export default function Countries() {
  const [searchCountry, setSearchCountry] = useState("");
  const [selectedRegion, setSelectedRegion] = useState("");
  const [selectedSubregion, setSelectedSubregion] = useState("");
  const [countries, setCountries] = useState([]);
  const [sortBy, setSortBy] = useState("");
  const [sortOrder, setSortOrder] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    setIsLoading(true);
    try {
      setTimeout(() => {
        setCountries(Data);
        setIsLoading(false);
      }, 1000);
    } catch (error) {
      setIsLoading(false);
      setError("Failed to fetch data. Please try again later.");
    }
  }, []);

  const regions = Data.reduce((acc, country) => {
    acc.add(country.region);
    return acc;
  }, new Set());

  const subregions = Data.filter(
    (country) => country.region === selectedRegion
  ).reduce((acc, country) => {
    acc.add(country.subregion);
    return acc;
  }, new Set());

  const handleSearch = (country) => {
    setSearchCountry(country);
  };

  const handleRegionSelect = (region) => {
    setSelectedRegion(region);
    setSelectedSubregion("");
  };

  const handleSubregionSelect = (subregion) => {
    setSelectedSubregion(subregion);
  };
  const handleSort = (sortBy, sortOrder) => {
    setSortBy(sortBy);
    setSortOrder(sortOrder);
  };

  const filterCountries = (search, region, subregion, sortBy, sortOrder) => {
    let filteredData = countries;

    if (search) {
      filteredData = filteredData.filter((country) =>
        country.name.common.toLowerCase().includes(search.toLowerCase())
      );
    }
    if (region) {
      filteredData = filteredData.filter(
        (country) => country.region.toLowerCase() === region.toLowerCase()
      );
    }
    if (subregion) {
      filteredData = filteredData.filter(
        (country) => country.subregion.toLowerCase() === subregion.toLowerCase()
      );
    }
    if (sortBy && sortOrder) {
      filteredData = filteredData.sort((a, b) => {
        if (sortOrder === "asc") {
          return a[sortBy] - b[sortBy];
        } else if (sortOrder === "desc") {
          return b[sortBy] - a[sortBy];
        }
      });
    }
    return filteredData;
  };

  const finalCountriesList = filterCountries(
    searchCountry,
    selectedRegion,
    selectedSubregion,
    sortBy,
    sortOrder
  );

  return (
    <div className="countryBox">
      <div className="menubar">
        <div className="search">
          <SearchInput onSearch={handleSearch} />
        </div>

        <div className="filterDiv">
          <div className="filterBox">
            <FilterCountry
              onSelectRegion={handleRegionSelect}
              regions={regions}
              filterBy={"Region"}
            />
            <FilterCountry
              onSelectRegion={handleSubregionSelect}
              regions={subregions}
              filterBy={"Sub-Region"}
            />
          </div>
          <div className="sortControls">
            <Sort onSelectSort={handleSort} sortBy={"population"} />
            <Sort onSelectSort={handleSort} sortBy={"area"} />
          </div>
        </div>
      </div>

      {isLoading && <div className="notFound">Loading...</div>}
      {error && <div className="notFound">Error: {error}</div>}
      {!isLoading && finalCountriesList.length === 0 && (
        <div className="notFound">No such countries found.</div>
      )}

      <div className="countryDetails">
        {finalCountriesList.map((country) => (
          <Link to={`Country/${country.name.common}`} key={country.name.common}>
            <div className="countryCard" key={country.name.common}>
              <div className="countryFlag">
                <img src={country.flags.png} alt="" />
              </div>
              <div className="countryData">
                <h2>{country.name.common}</h2>
                <h3>
                  <span>Population:</span> {country.population}
                </h3>
                <h3>
                  <span>Region: </span>
                  {country.region}
                </h3>
                <h3>
                  <span>Capital: </span>
                  {country.capital}
                </h3>
              </div>
            </div>
          </Link>
        ))}
      </div>
    </div>
  );
}
